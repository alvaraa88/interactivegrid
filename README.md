# Interactive Grid

Hello, Thanks for checking out my InteractiveGridApp.
Here are some of the main points I would like to express as the objective of this project.
1) MVC Framework
2) JavaFX fundamentals(GUI fundamentals)
3) Fundamental GUI interactions

## Getting started:
* This small program consists of one folder containing an .fxml file, controller, Square class and the main class.
>The **interactiveGrid.fxml** is responsible for the view 
1) interactiveGrid.fxml
    1) Not necessary but should be used as this is how Javafx was intended to be used.
>The **InteractiveGridController** is responsible for the interaction between 
> the backend and front end code.
1) InteractiveGridController
   1) This class contains the functionality and is a vital part of javaFX framework
   2) This class glues together the backend and front end development
>The **Square** is responsible for the building of the grid and interactions with each individual piece.
1) Square
   1) maintains the construction of a square.
   2) holds methods for interaction and drawing of squares

### Author and website:
1) [Alexander Alvara](https://alexanderalvara.com/resume)
2) [Gitlab](https://gitlab.com/alvaraa88/interactivegrid)
3) [LinkedIn](https://www.linkedin.com/in/alexander-alvara-4132a3a7/)
