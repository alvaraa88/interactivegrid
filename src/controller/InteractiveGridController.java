package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class InteractiveGridController implements Initializable {
    @FXML
    Pane gridPane;

    /**----------------
     * Local Variables
     ----------------*/
    private int paneSize = 200; //size of pane --> 200 x 200 at the moment
    private int squarePieces = 10; //__ NxN grid Squares

    private int squareSize = paneSize /squarePieces; // creates 10x10 squares to fit a 200x200 pane

    private ArrayList<Square> squares;

    private Rectangle[][] grid;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        squares = new ArrayList<>();
        grid = new Rectangle[squarePieces][squarePieces];
        //making the grid.
        for(int i = 0; i < paneSize; i += squareSize){
            for(int j = 0; j < paneSize; j += squareSize){
                Rectangle rect = new Rectangle(i, j, squareSize, squareSize);
                //Math WHY?
                // n/SquareSize --> n goes to 200 in this case.. wayyy too big, n/SquareSize guarantees correct positions
                grid[ i / squareSize][ j / squareSize] = rect;
                rect.setFill(Color.WHITE);
                rect.setStroke(Color.BLACK);
                gridPane.getChildren().add(rect);
            }
        }
        for (int i = 0; i < 5; ++i){
            Circle c = new Circle();
            c.setFill(Color.GREEN);
            c.setStroke(Color.BLACK);
            //Math WHY?
            //squaresize/3.0?! because we want the radius to be smaller than the square size for the circle!!
            double radius = squareSize / 3.0;

            //More Math! wwhooo
            // 1) (int)(Math.random * squarePieces) --> will positionally generate 0 to N(squarePieces) spots on a grid.
            // 2) squareSize/2 + SquareSize --> will set a circles position in the middle of a square in any square spot.
            int posX = squareSize/2 + squareSize * (int)(Math.random() * squarePieces);
            int posY = squareSize/2 + squareSize * (int)(Math.random() * squarePieces);
            Square square = new Square(posX, posY, radius, c);
            squares.add(square);

            //add functionality
            c.setOnMousePressed(event -> pressed(event, square));
            c.setOnMouseDragged(event -> dragged(event, square));
            c.setOnMouseReleased(event -> release(event, square));

            gridPane.getChildren().add(c);
            square.draw();
        }
    }
    public void pressed(MouseEvent event, Square square){
        square.setColor(Color.GREENYELLOW);
    }

    public void dragged(MouseEvent event, Square square){
        square.setX(square.getX() + event.getX());
        square.setY(square.getY() + event.getY());
        square.draw();
    }

    public void release(MouseEvent event, Square square){
        //MATH WHY??
        // 1) (int)square.getX() / squareSize; --> is equivalent to (int)(Math.random() * squarePieces);
        // 2) squareSize/2 + squareSize --> look at posX comments for explanation.
        int gridX = (int)square.getX() / squareSize;
        int gridY = (int)square.getY() / squareSize;
        grid[gridX][gridY].setFill(Color.RED);
        square.setX(squareSize/2 + squareSize * gridX);
        square.setY(squareSize/2 + squareSize * gridY);
        square.draw();
    }
}
