package controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class InteractiveGridApp extends Application {


    public static Stage stg;
    @Override
    public void start(Stage primaryStage) throws IOException {
        stg = primaryStage;

        Parent root = FXMLLoader.load(getClass().getResource("interactiveGrid.fxml"));

        Scene scene = new Scene(root);
        primaryStage.setTitle("Interactive Grid");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void changeScene(String fxml) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(fxml));
        stg.getScene().setRoot(root);
    }

    public static void main(String[] args){
        launch(args);
    }
}
