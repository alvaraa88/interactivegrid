package controller;

/**
 * test
 */
public class Directions {

    public static final int[][] DIRECTIONS = {
            {-1, 1}, {-1, 0}, {-1, 1},
            { 0, -1},         { 0, 1},
            { 1, -1}, { 1, 0}, { 1, 1}
    };

    public static int[][] getNeighbors(int col, int row){
        int[][] neighbors = new int[DIRECTIONS.length][2];

        for (int i = 0; i < DIRECTIONS.length; i++){
            neighbors[i][0] = col + DIRECTIONS[i][0];
            neighbors[i][1] = row + DIRECTIONS[i][1];
        }
        return neighbors;
    }
}
